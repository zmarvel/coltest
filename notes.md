*   Climate a function of elevation and latitude
*   Each cell has a nutrient content
    *   For producing food
    *   Nutrient content improved by compost/fertilizer
    *   Limits population growth
    *   Also dependent on climate
*   Some assumptions about the planet:
    *   It has water
    *   It has high carbon dioxide concentration (random value within a range)
*   Water below a certain elevation
    *   Also based on latitude/biome—need some way to generate "seas"
    *   Biome can also determine rainfall
    *   Assumes planet already has water
*   Carbon dioxide concentration in the atmosphere could also be an interesting
    concept
    *   More plants reduce carbon dioxide concentration. Perhaps this is another
        way to have population controls
    *   More people produce more carbon dioxide
    *   Special gear may be necessary at first, but once oxygen content has been
        increased in the atmosphere (how much do plants affect this?) perhaps
        not necessary
*   Crops
    *   Seeds brought from Earth
    *   If temperatures/biomes are different, the latitude at which they ideally
        grow on Earth may not be the same as this planet.
*   Conflict
    *   Aliens?
    *   Once population reaches a certain point, conflicts between different
        groups
*   Scale
    *   The size of the board should probably be about 2000 by 2000, with each
        cell representing a 10 km by 10 km area. On the other hand, that will
        probably take some time to run, unless I can significantly optimize the
        game of life stuff. Maybe the scale doesn't have to be 100% true.
*   Character types
    *   Pioneer (laser rifle)
        *   Can move more spaces than other players
        *   Can train into a profession
    *   Farmer (hoe)
        *   Grows crops, of course
        *   Should there be different types?
        *   Maybe with a different item he/she can do different things
    *   Scientist
        *   Should there be different types?
        *   Maybe with a different item he/she can do different things
    *   Engineer
        *   Should there be different types?
        *   Maybe with a different item he/she can do different things
    *   Mechanic
    *   Factory worker
    *   Militant
    *   Prisoner
        *   Unskilled
