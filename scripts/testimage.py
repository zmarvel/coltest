import subprocess
import os
import glob

from coltest import board, world

W, H = 200, 200

def write_elevation(seed):
    maker = world.ElevationLayerMaker(seed, W, H)
    maker.generate()
    maker.write_to_image('elevation_layer-{}.png'.format(seed))
    return maker

def write_water(seed, water_level, layers):
    maker = world.WaterLayerMaker(water_level, layers, W, H)
    maker.generate()
    maker.write_to_image('water_layer-{}.png'.format(seed))
    return maker

def write_climate(seed, layers):
    maker = world.ClimateLayerMaker(layers, W, H)
    maker.generate()
    maker.write_to_image('climate_layer-{}.png'.format(seed))
    return maker

def write_nutrient(seed, layers):
    maker = world.NutrientLayerMaker(layers, W, H)
    maker.generate()
    maker.write_to_image('nutrient_layer-{}.png'.format(seed))

def do_steps(n, seed):

    b = board.Board(W, H)
    b.make_terrain(seed)

    files = []
    for i in range(0, n):
        fname = '{:02}-{:02}.png'.format(seed, i)
        b.write_to_image(fname)

    subprocess.check_call(['convert', '-delay', '50', '-loop', '0', '*.png', '{}.gif'.format(seed)])
    pngs = glob.glob('*.png')
    for p in pngs:
        os.remove(p)

for seed in range(0, 2):
    layers = dict()
    em = write_elevation(seed)
    layers['elevation'] = em.layer
    wm = write_water(seed, 5, layers)
    layers['water'] = wm.layer
    cm = write_climate(seed, layers)
    layers['climate'] = cm.layer
    # nm = write_nutrient(seed, layers)
    # layers['nutrient'] = nm.layer
