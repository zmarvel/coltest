# board.py

import datetime
import copy

from PIL import Image

from coltest import world as w
from coltest import util

class Board():
    """The :py:class:`Board`, which keeps track of the state of the world.

    :param width: The width of the board in tiles.
    :param height: The height of the board in tiles.
    :param timedelta: How much does each tick advance the time? (Defaults to
        one month.
    :type width: int
    :type height: int
    :type timedelta: int
        """

    #: The current date. This is a :py:class:`datetime.datetime`.
    #:     
    #: This time is for all the layers in the :py:class:`Board`. With this in
    #: mind, another :py:class:`Board` could represent another place with a
    #: different terrain and different entities.
    time = datetime.datetime(2492, 1, 1)

    #: The month, as an :py:obj:`int`. This gets automatically updated with the
    #: date.
    month = time.month

    #: The year, as an :py:obj:`int`. This gets automatically updated with the
    #: date.
    year = time.year

    #: This reflects whose turn it is with an :py:obj:`int`. It will probably be
    #: an index into a list of entities.
    conch = None

    def __init__(self, width=24, height=16, timedelta=1):
        self.WIDTH = width
        self.HEIGHT = height

        self.timedelta = timedelta      # in months

        self.terrain = None

        self.layers = dict()

    def __iter__(self):
        return self

    def __next__(self):
        """Return the "next" board. In other words, advance the board: increment
        its time, move its entities, and so on.
        """

        next_month = self.month + 1
        if next_month > 12:
            next_month = 1
            self.time = self.time.replace(year=self.year+1)
        self.time = self.time.replace(month=next_month)
        self.month = self.time.month
        self.year = self.time.year

        return self

    def make_layers(self, seed=None):
        """
        Generate the different initial layers of the board, then put them in
        attributes of :py:class:`Board`.

        Updates :py:attr:`Board.layers`. 

        :param seed: Optional seed for reproducible world generation.
        :type seed: int, float
        """

        em = w.ElevationLayerMaker(seed, self.WIDTH, self.HEIGHT)
        em.generate()
        self.layers['elevation'] = em.layer
        wm = write_water(seed, 5, layers)
        self.layers['water'] = wm.layer
        cm = write_climate(seed, layers)
        self.layers['climate'] = cm.layer
        nm = write_nutrient(seed, layers)
        self.layers['nutrient'] = nm.layer
