import random as rand
import abc
import itertools as it

import yaml
from PIL import Image

from coltest import util

def terrain_types():
    with open('data/world/terrain.yaml', 'r') as f:
        terrain = yaml.load(f)
    return terrain

class LayerMaker(metaclass=abc.ABCMeta):
    def __init__(self, width=24, height=16):
        self.WIDTH = width
        self.HEIGHT = height
        self.layer = []
        self._initialize_layer()
    
    def _get_cell(self, x, y):
        i = (y * self.WIDTH) + x
        return self.layer[i]

    def _set_cell(self, x, y, value):
        i = (y * self.WIDTH) + x
        self.layer[i] = value
        return value

    @abc.abstractmethod
    def _initialize_layer(self):
        return False

    @abc.abstractmethod
    def generate(self):
        return None

    @abc.abstractmethod
    def write_to_image(self, filename):
        return None
    
class ElevationLayerMaker(LayerMaker):
    """The terrain generator. It uses Conway's Game of Life for generation: if
    a cell is alive, add one to the elevation of that cell. The number of
    iterations of the game allows us to define a maximum elevation.

    :param seed: The random seed, which allows for reproducible
        worlds.
    :param width: The width of the layer to generate.
    :param height: The height of the layer to generate.
    :type seed: int, float
    :type width: int
    :type height: int
    """

    def __init__(self, seed=None, width=24, height=16):
        super().__init__(width, height)
        self.seed = seed
        self.gol = util.GameOfLife(seed, width, height)

    def _initialize_layer(self):
        for x in range(0, self.WIDTH * self.HEIGHT):
            self.layer.append(0)

    def generate(self):
        """Using Conway's Game of Life, fill :py:attr:self.layer with a height
        map.
        """

        for g in it.islice(iter(self.gol), 0, 20):
            for x, y in it.product(range(self.WIDTH), range(self.HEIGHT)):
                cell = self._get_cell(x, y)
                if g[x][y]:
                    self._set_cell(x, y, cell + 1)

        return self.layer

    def write_to_image(self, filename):
        array = []
        for c in self.layer:
            val = int((c / 20.0) * 255)
            array.append((val, val, val))
        i = Image.new('RGB', (self.WIDTH, self.HEIGHT))
        i.putdata(array)
        i.save(filename)

class WaterLayerMaker(LayerMaker):
    """The water generator. It fills the all cells below a given elevation with
    water. This is represented as an array in which True signifies a cell is
    filled with water.

    :param water_level: The elevation below which a cell is filled with water.
    :param layers: A dict of the layers already generated (expecting only
        elevation)
    :param width: The width of the layer.
    :param height: The height of the layer.
    :type water_level: int
    :type layers: dict
    :type width: int
    :type height: int
    """

    def __init__(self, water_level, layers, width=24, height=16):
        super().__init__(width, height)
        self.water_level = water_level
        self.elevation_layer = layers['elevation']

    def _initialize_layer(self):
        self.layer = [False for _ in range(0, self.WIDTH * self.HEIGHT)]
        return self.layer

    def generate(self):
        for i, c in enumerate(self.elevation_layer):
            if c < self.water_level:
                self.layer[i] = True
            else:
                self.layer[i] = False
    
    def write_to_image(self, filename):
        array = []
        for c in self.layer:
            if c:
                array.append((0, 0, 0))
            else:
                array.append((255, 255, 255))
        i = Image.new('RGB', (self.WIDTH, self.HEIGHT))
        i.putdata(array)
        i.save(filename)

class ClimateLayerMaker(LayerMaker):
    """Precipitation generator. This is based largely on latitude and elevation.
    The climate types are defined in :py:attr:ClimateLayerMaker.types and are
    pulled from https://en.wikipedia.org/wiki/Climate#Climate_classification.

    :param layers: A dict of the layers already generated
        (dependent on elevation).
    :param width: The width of the layer.
    :param height: The height of the layer.
    :type layers: dict
    :type width: int
    :type height: int
    """

    #: Types of climate as a list. These should be ordered by distance from the
    #: equator.
    types = [
            'tropical',
            'dry',
            'moderate',
            'subpolar',
            'polar',
            ]

    def __init__(self, layers, width=24, height=16):
        super().__init__(width, height)
        self.elevation_layer = layers['elevation']
        self.water_layer = layers['water']

    def _initialize_layer(self):
        self.layer = [None for _ in range(0, self.WIDTH * self.HEIGHT)]
        return self.layer
    
    def generate(self):
        equator = self.HEIGHT // 2
        nclimates = len(self.types)
        interval = equator // nclimates
        min_elev = min(self.elevation_layer)
        max_elev = max(self.elevation_layer)
        elev_range = max_elev - min_elev

        for i, c in enumerate(self.elevation_layer):
            x = i % self.WIDTH 
            y = i // self.WIDTH
            dist = abs(y - equator)
            climate = dist // interval
            if c > elev_range // 2 and climate != nclimates - 1:
                climate += 1
            self.layer[i] = climate

    def write_to_image(self, filename):
        nclimates = len(self.types)
        array = []
        for c in self.layer:
            val = int((c / nclimates) * 255)
            array.append((val, val, val))
        i = Image.new('RGB', (self.WIDTH, self.HEIGHT))
        i.putdata(array)
        i.save(filename)

class NutrientLayerMaker(LayerMaker):
    """Nutrient layer generator. The idea is the player arrives with a group of
    people on a planet with little or no life. Since there's no life there, no
    organic nutrients are available. The nutrient content of the planet is low,
    so crops will have low yields without the use of fertilizer. Later, the
    inhabitants of the planet can compost waste to increase the nutrient content
    of the soil as well. Initially, this is based on climate.

    :param layers: A dict of the layers already generated (dependent on
        climate).
    :param width: The width of the layer.
    :param height: The height of the layer.
    :type layers: dict
    :type width: int
    :type height: int
    """

    def __init__(self, layers, width, height):
        super().__init__(width, height)
        self.climate_layer = layers['climate']

    def _initialize_layer(self):
        self.layer = [0 for _ in range(0, self.WIDTH * self.HEIGHT)]
        return self.layer

    def generate(self):
        climate_types = ClimateLayerMaker.types
        for i, c in enumerate(self.climate_layer):
            if climate_types[c] == 'tropical':
                self.layer[i] = 5
            elif climate_types[c] == 'dry':
                self.layer[i] = 1
            elif climate_types[c] == 'moderate':
                self.layer[i] = 4
            elif climate_types[c] == 'subpolar':
                self.layer[i] = 2
            elif climate_types[c] == 'polar':
                self.layer[i] = 0
        return self.layer

    def write_to_image(self, filename):
        nutrient_range = max(self.layer) - min(self.layer)
        array = []
        for c in self.layer:
            val = int((c / nutrient_range) * 255)
            array.append((val, val, val))
        i = Image.new('RGB', (self.WIDTH, self.HEIGHT))
        i.putdata(array)
        i.save(filename)
