# util.py
import random as rand

def in_layer(layer, x, y):
    """
    Is the point a valid index into a layer of the board?

    :param x: *x*-index from the left.
    :param y: *y*-index from the top.
    :type x: int
    :type y: int
    """

    width = len(layer)
    height = len(layer[0])

    if x < 0 or x + 1 >= width:
        return False
    elif y < 0 or y + 1 >= height:
        return False
    else:
        return True

def living_neighbors(layer, x, y):
    """How many of the cell's neighbors are alive?

    :param x: *x*-index from the left.
    :param y: *y*-index from the top.
    :type x: int
    :type y: int
    :return: Number of neighboring cells that are alive.
    :rtype: int
    """

    width = len(layer)
    height = len(layer[0])
    count = 0
    for i in range(-1, 2):
        for j in range(-1, 2):
            if (i, j) == (0, 0):
                pass
            if layer[(x + i) % width][(y + j) % height]:
                count += 1
    return count

def cell_lives(layer, x, y):
    """Check how many living neighbors the cell has. Should it be alive?

    :param x: *x*-index from the left.
    :param y: *y*-index from the top.
    :type x: int
    :type y: int
    :return: Does it live?
    :rtype: bool
    """
    starve = 3
    birth = 5

    count = living_neighbors(layer, x, y)
    if layer[x][y] and count > starve:
        return True
    elif not layer[x][y] and count > birth:
        return True
    else:
        return False

def cell_lives_maker(rules):
    def cell_lives_generic(layer, x, y):
        count = living_neighbors(layer, x, y)
        cell = layer[x][y]
        for r in rules:
            if r(cell, count):
                return True
        return False
    return cell_lives_generic

rules_gol = [
        lambda cell, count: cell and (count == 2 or count == 3),
        lambda cell, count: not cell and count == 3
        ]
cell_lives_gol = cell_lives_maker(rules_gol)

class GameOfLife():
    def __init__(self, seed=None, width=24, height=16, alive_chance=0.5):
        self.seed = seed
        self.WIDTH = width
        self.HEIGHT = height
        self.alive_chance = alive_chance
        self.grid = []
        self._initialize_grid()
        self.steps = 0
        self.cell_lives_rules = rules_gol
        self.cell_lives = cell_lives_gol

    def _initialize_grid(self):
        """Randomly populate the layer, based on the factor
        :py:attr:`self.alive_chance`.
        """

        rand.seed(self.seed)
        for x in range(0, self.WIDTH):
            self.grid.append([])
            for y in range(0, self.HEIGHT):
                r = rand.random()
                self.grid[x].append(r < self.alive_chance)

    def __iter__(self):
        return self

    def __next__(self):
        """Check to see if each cell lives or not in the next iteration of the
        grid, and change its status appropriately.
        """
        if self.steps != 0:
            grid = []
            for x in range(0, self.WIDTH):
                grid.append([self.cell_lives(self.grid, x, y)
                    for y in range(0, self.HEIGHT)])
            self.grid = grid

        self.steps += 1

        return self.grid
