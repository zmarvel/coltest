from setuptools import setup, find_packages

with open('README') as f:
    readme = f.read()

setup(
        name='coltest',
        version='0.0.1dev0',
        description='A simple game based loosely on freecol.',
        url='https://bitbucket.org/zmarvel/coltest',
        author='Zack Marvel',
        author_email='',
        license='MIT',
        classifiers=[
            'Development Status :: 1 - Planning',
            'Programming Language :: Python :: 3'
            ],
        keywords='coltest games',
        packages=find_packages(exclude=['docs', 'tests']),
        package_data={
            'world': 'data/world/*.dat'
            },
        install_requires=[
            'pyyaml',
            'pillow'
            ],
        )
