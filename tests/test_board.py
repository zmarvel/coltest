# tests/test_board.py

import coltest.board
import unittest

class TestBoard(unittest.TestCase):
    WIDTH = 4
    HEIGHT = 4

    def setUp(self):
        self.board = coltest.board.Board(self.WIDTH, self.HEIGHT)

    def test_constructor(self):
        self.assertEqual(self.board.year, 1492)
        self.assertEqual(self.board.month, 1)
        self.assertEqual(self.board.WIDTH, self.WIDTH)
        self.assertEqual(self.board.HEIGHT, self.HEIGHT)
        self.assertEqual(self.board.timedelta, 1)

    def test_next(self):
        # This only affects time so far.
        first_time = self.board.time
        first_month = self.board.month
        first_year = self.board.year
        next(self.board)
        second_time = self.board.time
        second_month = self.board.month
        second_year = self.board.year
        self.assertNotEqual(first_month, second_month)
        self.assertEqual(first_year, second_year)
        self.assertEqual(first_month, 1)
        self.assertEqual(first_year, 1492)
        self.assertEqual(second_month, 2)
        self.assertEqual(second_year, 1492)
        for m in range(0, 11):
            next(self.board)
        second_time = self.board.time
        second_month = self.board.month
        second_year = self.board.year
        self.assertEqual(first_month, second_month)
        self.assertNotEqual(first_year, second_year)
        self.assertEqual(second_year, 1493)

class TestTerrainLayerMaker(unittest.TestCase):
    WIDTH = 4
    HEIGHT = 4

    def setUp(self):
        self.maker = coltest.board.TerrainLayerMaker(1234, self.WIDTH, self.HEIGHT)

    def test_constructor(self):
        self.assertEqual(self.maker.seed, 1234)
        self.assertIsInstance(self.maker.terrain, list)
        self.assertIsInstance(self.maker.terrain[0], list)
        self.assertEqual(len(self.maker.terrain), self.WIDTH)
        self.assertEqual(len(self.maker.terrain[0]), self.HEIGHT)

    def test_initialize_board(self): # not done
        self.maker._initialize_board()
        # [False, False, True,  False]
        # [False, False, False, True ]
        # [False, True,  True,  False]
        # [True,  False, False, True ]
